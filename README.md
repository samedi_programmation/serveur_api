############################
# Les Samedi Programmation #
############################


Samedi 11/07/2020 : Serveur & API
=================================

Un petit projet en Golang pour démontrer les échanges de données entre une mini page Web, et un serveur répondant aux appels de la page.


- Comment créer un serveur Web
- Comment mettre à disposition le contenu d'un répertoire
- Comment recevoir des données d'une page Web (GET, POST)
- Comment traiter les données reçues de la page Web

Pour compiler et obtenir un exécutable, go build main.go token.go

Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex