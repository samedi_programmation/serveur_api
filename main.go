package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gempir/go-twitch-irc"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
)

const CHANNEL string = "adaralex"
var client *twitch.Client

type ReponseAleatoire struct {
	Question string `json:"question"`
	Reponses []string `json:"reponses"`
}

func main() {
	var dir string

	flag.StringVar(&dir, "dir", "./html/", "the directory to serve files from. Defaults to the current dir")
	flag.Parse()
	r := mux.NewRouter()

	client = twitch.NewClient("Wizardy_bot", GetAuthToken())

	// This will serve files under http://localhost:8000/static/<filename>
	r.HandleFunc("/api/input/{value}", processInput).Methods("GET")
	r.HandleFunc("/api/query", processQuery).Queries("channel", "{channel}", "message", "{message}").Methods("GET")
	r.HandleFunc("/api/spam", processPost).Methods("POST")
	r.PathPrefix("/").Handler(http.FileServer(http.Dir(dir)))

	srv := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:8000",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	client.Join(CHANNEL)

	go func() {
		err := client.Connect()
		if err != nil {
			panic(err)
		}
	}()

	log.Fatal(srv.ListenAndServe())
}

func processInput(reponse http.ResponseWriter, requete *http.Request) {
	vars := mux.Vars(requete)
	reponse.WriteHeader(http.StatusOK)
	//fmt.Fprintf(reponse, "Valeur reçue: %v\n", vars["value"])
	fmt.Printf("Valeur reçue: %v\n", vars["value"])
	client.Say(CHANNEL, vars["value"])
}


func processQuery(reponse http.ResponseWriter, requete *http.Request) {
	vars := mux.Vars(requete)
	channel := vars["channel"]
	message := vars["message"]

	fmt.Printf("Channel = %s, Message = %s\n", channel, message)
	client.Say(channel, message)

	listeReponses := make([]string, 0)

	listeReponses = append(listeReponses, "A) Un streamer")
	listeReponses = append(listeReponses, "B) Un extra-terrestre")
	listeReponses = append(listeReponses, "C) Un viewer")
	listeReponses = append(listeReponses, "D) La réponse D")

	reponseAleatoire := ReponseAleatoire{
		Question: "Qui est " + channel + " ?",
		Reponses: listeReponses,
	}

	json.NewEncoder(reponse).Encode(reponseAleatoire)
	reponse.WriteHeader(http.StatusOK)

}

type ReponseJson struct {
	Count string `json:"count"`
	Ban bool `json:"ban"`
}

func processPost(reponse http.ResponseWriter, requete *http.Request) {
	reponse.WriteHeader(http.StatusOK)

	var post ReponseJson
	_ = json.NewDecoder(requete.Body).Decode(&post)

	fmt.Printf("%+v\n", post)

}